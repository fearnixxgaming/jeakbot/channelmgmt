package de.fearnixx.jeak.channelmgmt;

import de.fearnixx.jeak.channelmgmt.commands.EnforceRegisterCommand;
import de.fearnixx.jeak.channelmgmt.commands.OverflowRegisterCommand;
import de.fearnixx.jeak.channelmgmt.enforcement.EnforcementService;
import de.fearnixx.jeak.channelmgmt.ondemand.ChannelDemandService;
import de.fearnixx.jeak.channelmgmt.overflow.OverflowManager;
import de.fearnixx.jeak.event.bot.IBotStateEvent;
import de.fearnixx.jeak.reflect.IInjectionService;
import de.fearnixx.jeak.reflect.Inject;
import de.fearnixx.jeak.reflect.JeakBotPlugin;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.jeak.service.command.ICommandService;
import de.fearnixx.jeak.service.event.IEventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@JeakBotPlugin(id = "de.fearnixx.jeak.channelmanagement", builtAgainst = "1.0.1", version = "1.3.0")
public class ChannelManagement {

    private static final Logger logger = LoggerFactory.getLogger(ChannelManagement.class);

    @Inject
    public IInjectionService injectionService;

    @Inject
    public IEventService eventService;

    @Inject
    public ICommandService commandService;

    private EnforcementService enforcementService = new EnforcementService();
    private EnforceRegisterCommand registerEnforceCommand = new EnforceRegisterCommand();
    private OverflowManager overflowManager = new OverflowManager();
    private OverflowRegisterCommand registerOverflowCommand = new OverflowRegisterCommand();
    private ChannelDemandService demandService = new ChannelDemandService();

    @Listener
    public void onPluginsLoaded(IBotStateEvent.IPluginsLoaded event) {
        injectionService.injectInto(enforcementService);
        eventService.registerListener(enforcementService);
        injectionService.injectInto(overflowManager);
        eventService.registerListener(overflowManager);
        injectionService.injectInto(demandService);
        eventService.registerListener(demandService);
        registerEnforceCommand.setService(enforcementService);
        injectionService.injectInto(registerEnforceCommand);
        registerOverflowCommand.setManager(overflowManager);
        injectionService.injectInto(registerOverflowCommand);
    }

    @Listener
    public void onPreInitialize(IBotStateEvent.IPreInitializeEvent event) {
        commandService.registerCommand("enforce-register", registerEnforceCommand);
        commandService.registerCommand("overflow-register", registerOverflowCommand);
    }
}
