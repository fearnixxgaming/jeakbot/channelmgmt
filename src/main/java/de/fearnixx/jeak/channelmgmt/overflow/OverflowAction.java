package de.fearnixx.jeak.channelmgmt.overflow;


import de.fearnixx.jeak.teamspeak.data.IDataHolder;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class OverflowAction {

    private List<Integer> forDeletion = new LinkedList<>();
    private List<OverflowCreateInfo> forCreation = new LinkedList<>();

    public void delete(Integer channelId) {
        forDeletion.add(channelId);
    }

    public void create(IDataHolder properties, List<String> enforcements, Map<String, Integer> permissions) {
        forCreation.add(new OverflowCreateInfo(properties, enforcements, permissions));
    }

    public List<Integer> getForDeletion() {
        return forDeletion;
    }

    public List<OverflowCreateInfo> getForCreation() {
        return forCreation;
    }
}
